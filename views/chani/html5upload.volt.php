<!-- TODO: Separate image upload and video as there's a distinction in type now -->
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<link href='http://fonts.googleapis.com/css?family=Source+Code+Pro:400,700,300|Shanti' rel='stylesheet' type='text/css'>
	<title>HTML5 File upload (IE fallback)</title>
	<?php echo $script; ?>
	<link href="<?php echo $this->url->get('public/css/chani/uniform.css'); ?>" rel="stylesheet" />
    <style type="text/css">
        body {
            margin: 0 1rem;
        }
        ul {
            list-style: none;
            margin: 0.5rem 0;
            padding: 0;
        }
    </style>
</head>
<body onload="html5upload.checkInputMultiple('<?php echo $sFormId; ?>')">

<div class="uniForm">

	<div class="col">
		<div class="twoCol"><label class="fullWidth"><strong>Upload bestanden</strong></label></div>
		<div class="twoCol"><label class="fullWidth" for="<?php echo $sFormId; ?>_VideoUrl"><strong>Youtube / Vimeo</strong></label></div>
	</div>
	<div class="col">
		<div class="twoCol">
			<!-- IMPORTANT:  FORM's enctype must be "multipart/form-data" -->
			<form id="<?php echo $sFormId; ?>" method="post" action="<?php echo $url; ?>" enctype="multipart/form-data" onsubmit="return html5upload.uploadFiles(this.id)">
				<input type="hidden" name="formId" value="<?php echo $sFormId; ?>" />
				<label for="<?php echo $sFormId; ?>Files" class="fullWidth">Kies bestanden</label>
				<input name="<?php echo $sFormId; ?>Files[]" id="<?php echo $sFormId; ?>Files" type="file" multiple="multiple" onchange="html5upload.listFiles(this)" />
				<div id="<?php echo $sFormId; ?>Fallback" class="uploadFallback" style="display: none;">
					<div id="<?php echo $sFormId; ?>FallbackInputs" class="inputs">

					</div>
					<a href="#" onclick="html5upload.addFileInput('<?php echo $sFormId; ?>FallbackInputs','<?php echo $sFormId; ?>Files[]')"><?php echo $t->_('addFile'); ?></a>
				</div>
				<div class="clear"></div>
                <div class="buttons">
                    <button type="submit" name="<?php echo $sFormId; ?>Submit" class="formSubmit"><?php echo $t->_('uploadFiles'); ?></button>
                    <div class="clear"></div>
                </div>
                <p>Upload progress:</p>
				<progress id="<?php echo $sFormId; ?>Progress" min="0.0" max="100.0" value="0"></progress>
                <ul class="clear" id="<?php echo $sFormId; ?>FilesList"></ul>
			</form>
		</div>
		<div class="twoCol">
			<form id="<?php echo $sFormId; ?>_Video" method="post" action="<?php echo $videoUrl; ?>" onsubmit="return html5upload.addVideo(this.id)">
				<input type="hidden" name="formId" value="<?php echo $sFormId; ?>" />
				<label for="<?php echo $sFormId; ?>_VideoUrl">Media URL:</label>
				<input data-id="<?php echo $iModelId; ?>" type="text" name="<?php echo $sFormId; ?>_VideoUrl" id="<?php echo $sFormId; ?>_VideoUrl" value="" />
				<div class="buttons">
					<button type="submit" class="formSubmit" name="<?php echo $sFormId; ?>_VideoSubmit" id="<?php echo $sFormId; ?>_VideoSubmit" ><?php echo $t->_('save'); ?></button>
				</div>
			</form>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
</body>
</html>