<h1>Media library</h1>
<div class="media-library-navigation">
    <ul>
        <li><a title="Root">Root</a></li>
        <li>&gt;</li>
        <li><a title="Folder">Folder</a></li>
    </ul>
    <div class="clear"></div>
</div>
<div class="media-library">
    <div class="media">
        <i class="fa fa-4x fa-folder"></i>
        <i class="fa fa-4x fa-folder-open"></i>
        <span>Folder</span>
    </div>
    <div class="media">
        <i class="fa fa-4x fa-folder"></i>
        <i class="fa fa-4x fa-folder-open"></i>
        <span>Folder</span>
    </div>
    <div class="media">
        <i class="fa fa-4x fa-folder"></i>
        <i class="fa fa-4x fa-folder-open"></i>
        <span>Folder</span>
    </div>
    <div class="media">
        <i class="fa fa-4x fa-folder"></i>
        <i class="fa fa-4x fa-folder-open"></i>
        <span>Folder</span>
    </div>
    <div class="media">
        <i class="fa fa-4x fa-folder-o"></i>
        <i class="fa fa-4x fa-folder-open-o"></i>
        <span>Add folder</span>
    </div>
    <div class="media">
        <img src="/public/img/cowAvatar.jpg" />
    </div>
    <div class="media">
        <i class="fa fa-4x fa-video-camera"></i>
    </div>
    <div class="media">
        <img src="/public/img/cowAvatar.jpg" />
    </div>
    <div class="media">
        <i class="fa fa-4x fa-image"></i>
    </div>
    <div class="media">
        <img src="/public/media/stock/P3240034.JPG" />
    </div>
    <div class="media">
        <img src="/public/media/stock/P3240035.JPG" />
    </div>
    <div class="media">
        <img src="/public/media/stock/P3250036.JPG" />
    </div>
    <div class="media">
        <img src="/public/media/stock/P3250037.JPG" />
    </div>
    <div class="media">
        <img src="/public/media/stock/P3250038.JPG" />
    </div>
    <div class="media">
        <img src="/public/media/stock/BitOfAByte.png" />
    </div>
</div>
<div class="buttons">
    <button class="cancel" data-target="media" onclick="return Chani.toggleModalContent(this,false,false,event);">Close</button>
    <button class="save">Add</button>
</div>