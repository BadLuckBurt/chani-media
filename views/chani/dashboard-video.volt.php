<h1>Video settings</h1>
<div id="dashboard-image">
    <div class="uniForm">
        <div class="twoCol">
            <label>URL: <span id="mediaUrl"></span></label>
        </div>
        <div class="twoCol">
            <label for="mediaVideoWidth">Width</label>
            <input type="text" id="mediaVideoWidth" name="iVideoWidth" value="" />
            <label for="mediaVideoHeight">Height</label>
            <input type="text" id="mediaVideoHeight" name="iVideoHeight" value="" />
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="buttons">
    <button data-target="image" class="save image-save">Close</button>
</div>