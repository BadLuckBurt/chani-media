<div class="media-options">
    <div class="media-buttons left">
        <a href="#" class="media-edit" data-id="{{media['id']}}" data-target="{{ media['type'] }}" title="Edit {{ media['type'] }}">
            <i class="fa fa-2x fa-pencil-square-o"></i>
        </a>
    </div>
    <div class="media-buttons right">
        <a href="#" class="media-delete" data-id="{{media['id']}}" title="Delete media">
            <i class="fa fa-2x fa-trash-o"></i>
        </a>
    </div>
</div>
<div class="media-content">
    {{ media['img'] }}
    {% if bVideo == true %}
    <input data-id="{{ media['id'] }}" data-column="sUrl" data-locale="false" type="hidden" id="sUrl{{ media['id'] }}" name="sUrl" value="{{ media['url'] }}" />
    <input data-id="{{ media['id'] }}" data-column="iVideoWidth" data-locale="false" type="hidden" id="iVideoWidth{{ media['id'] }}" name="iVideoWidth" value="{{ media['iWidth'] }}" />
    <input data-id="{{ media['id'] }}" data-column="iVideoHeight" data-locale="false" type="hidden" id="iVideoHeight{{ media['id'] }}" name="iVideoHeight" value="{{ media['iHeight'] }}" />
    {% else %}
    <input data-id="{{ media['id'] }}" data-column="sTitle" data-locale="true" type="hidden" id="sMediaTitle{{ media['id'] }}" value="{{ media['title'] }}" />
    <input data-id="{{ media['id'] }}" data-column="sAlt" data-locale="true" type="hidden" id="sMediaAlt{{ media['id'] }}" value="{{ media['alt'] }}" />
    {% endif%}
</div>