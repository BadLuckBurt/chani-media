<div class="media-options">
    <div class="media-buttons left">
        <a href="#" class="media-edit" data-id="<?php echo $media['id']; ?>" data-target="<?php echo $media['type']; ?>" title="Edit <?php echo $media['type']; ?>">
            <i class="fa fa-2x fa-pencil-square-o"></i>
        </a>
    </div>
    <div class="media-buttons right">
        <a href="#" class="media-delete" data-id="<?php echo $media['id']; ?>" title="Delete media">
            <i class="fa fa-2x fa-trash-o"></i>
        </a>
    </div>
</div>
<div class="media-content">
    <?php echo $media['img']; ?>
    <?php if ($bVideo == true) { ?>
    <input data-id="<?php echo $media['id']; ?>" data-column="sUrl" data-locale="false" type="hidden" id="sUrl<?php echo $media['id']; ?>" name="sUrl" value="<?php echo $media['url']; ?>" />
    <input data-id="<?php echo $media['id']; ?>" data-column="iVideoWidth" data-locale="false" type="hidden" id="iVideoWidth<?php echo $media['id']; ?>" name="iVideoWidth" value="<?php echo $media['iWidth']; ?>" />
    <input data-id="<?php echo $media['id']; ?>" data-column="iVideoHeight" data-locale="false" type="hidden" id="iVideoHeight<?php echo $media['id']; ?>" name="iVideoHeight" value="<?php echo $media['iHeight']; ?>" />
    <?php } else { ?>
    <input data-id="<?php echo $media['id']; ?>" data-column="sTitle" data-locale="true" type="hidden" id="sMediaTitle<?php echo $media['id']; ?>" value="<?php echo $media['title']; ?>" />
    <input data-id="<?php echo $media['id']; ?>" data-column="sAlt" data-locale="true" type="hidden" id="sMediaAlt<?php echo $media['id']; ?>" value="<?php echo $media['alt']; ?>" />
    <?php } ?>
</div>