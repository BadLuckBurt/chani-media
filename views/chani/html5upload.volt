<!-- TODO: Separate image upload and video as there's a distinction in type now -->
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<link href='http://fonts.googleapis.com/css?family=Source+Code+Pro:400,700,300|Shanti' rel='stylesheet' type='text/css'>
	<title>HTML5 File upload (IE fallback)</title>
	{{ script }}
	<link href="{{url('public/css/chani/uniform.css')}}" rel="stylesheet" />
    <style type="text/css">
        body {
            margin: 0 1rem;
        }
        ul {
            list-style: none;
            margin: 0.5rem 0;
            padding: 0;
        }
    </style>
</head>
<body onload="html5upload.checkInputMultiple('{{ sFormId }}')">

<div class="uniForm">

	<div class="col">
		<div class="twoCol"><label class="fullWidth"><strong>Upload bestanden</strong></label></div>
		<div class="twoCol"><label class="fullWidth" for="{{sFormId}}_VideoUrl"><strong>Youtube / Vimeo</strong></label></div>
	</div>
	<div class="col">
		<div class="twoCol">
			<!-- IMPORTANT:  FORM's enctype must be "multipart/form-data" -->
			<form id="{{sFormId}}" method="post" action="{{ url }}" enctype="multipart/form-data" onsubmit="return html5upload.uploadFiles(this.id)">
				<input type="hidden" name="formId" value="{{sFormId}}" />
				<label for="{{sFormId}}Files" class="fullWidth">Kies bestanden</label>
				<input name="{{sFormId}}Files[]" id="{{sFormId}}Files" type="file" multiple="multiple" onchange="html5upload.listFiles(this)" />
				<div id="{{sFormId}}Fallback" class="uploadFallback" style="display: none;">
					<div id="{{sFormId}}FallbackInputs" class="inputs">

					</div>
					<a href="#" onclick="html5upload.addFileInput('{{sFormId}}FallbackInputs','{{sFormId}}Files[]')">{{t._('addFile')}}</a>
				</div>
				<div class="clear"></div>
                <div class="buttons">
                    <button type="submit" name="{{sFormId}}Submit" class="formSubmit">{{t._('uploadFiles')}}</button>
                    <div class="clear"></div>
                </div>
                <p>Upload progress:</p>
				<progress id="{{sFormId}}Progress" min="0.0" max="100.0" value="0"></progress>
                <ul class="clear" id="{{sFormId}}FilesList"></ul>
			</form>
		</div>
		<div class="twoCol">
			<form id="{{sFormId}}_Video" method="post" action="{{ videoUrl }}" onsubmit="return html5upload.addVideo(this.id)">
				<input type="hidden" name="formId" value="{{sFormId}}" />
				<label for="{{sFormId}}_VideoUrl">Media URL:</label>
				<input data-id="{{iModelId}}" type="text" name="{{sFormId}}_VideoUrl" id="{{sFormId}}_VideoUrl" value="" />
				<div class="buttons">
					<button type="submit" class="formSubmit" name="{{sFormId}}_VideoSubmit" id="{{sFormId}}_VideoSubmit" >{{t._('save')}}</button>
				</div>
			</form>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
</body>
</html>