<h1>Upload file / process video URL</h1>
<div id="dashboard-upload">
    <div class="uniForm">
        <form id="imageUpload" method="post" action="" enctype="multipart/form-data" onsubmit="return html5upload.uploadFiles()">
            <div class="col">
                <div class="twoCol"><label><strong>Upload bestanden</strong></label></div>
                <div class="twoCol">
                    <input type="hidden" name="formId" value="imageUpload" />
                    <label for="imageUploadFiles" class="fullWidth">Kies bestanden</label>
                    <input name="imageUploadFiles[]" id="imageUploadFiles" type="file" multiple="multiple" onchange="html5upload.listFiles(this)" />
                    <div id="imageUploadFallback" class="uploadFallback" style="display: none;">
                        <div id="imageUploadFallbackInputs" class="inputs">

                        </div>
                        <a href="#" onclick="html5upload.addFileInput('imageUploadFallbackInputs','imageUploadFiles[]')">{{t._('addFile')}}</a>
                    </div>
                    <ul class="clear" id="imageUploadFilesList"></ul>
                </div>
                <div class="twoCol">
                    <label>Upload progress:</label>
                    <progress id="imageUploadProgress" min="0.0" max="100.0" value="0"></progress>
                    <div class="buttons">
                        <button type="submit" name="imageUploadSubmit" class="formSubmit">{{t._('uploadFiles')}}</button>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </form>

        <form id="videoUpload" method="post" action="" onsubmit="return html5upload.addVideo()">
            <div class="twoCol">
                <input type="hidden" name="formId" value="videoUpload" />
                <label class="fullWidth" for="videoUploadUrl"><strong>Youtube / Vimeo</strong></label>
                <input type="text" name="videoUploadUrl" id="videoUploadUrl" value="" />
                <div class="buttons">
                    <button type="submit" class="formSubmit" name="videoUploadSubmit" id="videoUploadSubmit" >{{t._('save')}}</button>
                </div>
            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>
<div class="buttons">
    <button data-target="media" class="cancel media-cancel">Cancel</button>
</div>