<h1>Image settings</h1>
<div id="dashboard-image">
    <div class="uniForm">
        <div class="twoCol">
            <label for="mediaTitle">Caption</label>
            <input type="text" id="mediaTitle" name="sTitle" value="" />
        </div>
        <div class="twoCol">
            <label for="mediaAlt">Alt</label>
            <input type="text" id="mediaAlt" name="sAlt" value="" />
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="buttons">
    <button data-target="image" class="cancel image-cancel">Close</button>
</div>