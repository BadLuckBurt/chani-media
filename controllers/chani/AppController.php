<?php
	namespace Media\Controllers\Chani;

	use \Core\Controllers\Chani\AppController AS Controller,
		\Phalcon\DI\FactoryDefault,
		\Phalcon\Translate\Adapter\NativeArray AS Translator,
		\Phalcon\Mvc\View\Simple,
		\Phalcon\Tag,
		\Phalcon\Image\Adapter\GD,
		\Media\Models\Chani\CmsMedia AS CmsMedia,
		\Media\Models\Chani\AppMedia AS Media;

	class AppController extends Controller {

		/**
		 * onConstruct is needed to overwrite the View-object
		 * when we instantiate this controller from within another controller
		 * otherwise, it would point to the views-folder of the module we instantiate this controller in
		 */
		public function onConstruct() {
			$aNameSpace = explode('\\',__NAMESPACE__);
			$this->view = new \Phalcon\Mvc\View\Simple();
			$this->view->registerEngines(array(
				".volt" => 'Phalcon\Mvc\View\Engine\Volt'
			));
			$this->view->setViewsDir(MODULE_DIR.'/'.strtolower($aNameSpace[0]).'/views/');
			$this->view->setDI(new FactoryDefault());
		}

		/**
		 * @return \Phalcon\Translate\Adapter\NativeArray
		 * Gets the translations for the language the user is currently using
		 * TODO: Tweak this function to handle non-existing translation locales and force it to use 'nl' instead of 'nl-NL'
		 */
		public function _getTranslation($sNameSpace = '')
		{

			//Determine the browser language of the user
			$sLanguage = $this->request->getBestLanguage();
			//Format returned for example: en-EN - we only need the first part
			$aLanguage = explode('-',$sLanguage);
			$sLanguage = $aLanguage[0];

			//If no namespace is provided, use the current one
			if($sNameSpace == '') {
				$sNameSpace = __NAMESPACE__;
			}

			//Build the regular path, the chani path and fallback (english) path using the namespace,
			$aNameSpace = explode('\\',$sNameSpace);
			$sPath = MODULE_DIR.'/'.strtolower($aNameSpace[0]).'/translations/';
			$sChaniPath = $sPath.'chani/';
			$sFallback = $sChaniPath.'en.php';
			$sPath .= $sLanguage.'.php';
			$sChaniPath .= $sLanguage.'.php';

			//Check if we have a translation file for that language, if not fall back to whatever is available
			$aMessages = null;
			if (file_exists($sPath)) {
				require $sPath;
			} elseif(file_exists($sChaniPath)) {
				// fallback to some default
				require $sChaniPath;
			} else {
				require $sFallback;
			}

			//Return a translation object
			return new Translator(array("content" => $aMessages));

		}

	}