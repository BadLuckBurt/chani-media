<?php

	namespace Media\Controllers\Chani;

	use \Core\Controllers\Chani\CmsController AS Controller,
		\Phalcon\DI\FactoryDefault,
		\Phalcon\Translate\Adapter\NativeArray AS Translator,
		\Phalcon\Mvc\View\Simple,
		\Phalcon\Tag,
		\Phalcon\Image\Adapter\GD,
		\Media\Models\Chani\CmsMedia,
		\Vimeo\Controllers\Chani\VimeoController AS VimeoController;

	class CmsController extends Controller {

		//TODO: REMOVE IF THERE ARE NO ERRORS
		/*
		//Fields and buttons for Media editing
		public $aFormFields = array(
			array(
				'type' => 'mediaSettings',
				'fields' => array(
					'sTitle' => array(
						'tag' => 'textField',
						'value' => '',
						'data-events' => 'blur',
						'data-blur' => 'saveValue',
						'data-locale' => 'true',
						'class' => 'media'
					),
					'sUrl' => array(
						'tag' => 'textField',
						'value' => '',
						'data-events' => 'blur',
						'data-blur' => 'saveValue',
						'data-locale' => 'false',
						'class' => 'media'
					),
					'sAlt' => array(
						'tag' => 'textField',
						'value' => '',
						'data-events' => 'blur',
						'data-blur' => 'saveValue',
						'data-locale' => 'true',
						'class' => 'media'
					),
					'sContent' => array(
						'tag' => 'textArea',
						'value' => '',
						'data-events' => 'blur',
						'data-blur' => 'saveValue',
						'data-locale' => 'true',
						'class' => 'media'
					)
				),
			)
		);
		public $aFormButtons = array(
			array(
				'type' => 'mediaButtons',
				'fields' => array(
					'cropper' => array(
						'data-id' => '',
						'data-events' => 'click',
						'data-click' => 'openCropper',
						'data-locale' => 'false',
						'class' => 'add media right'
					),
					'delete' => array(
						'data-id' => '',
						'data-events' => 'click',
						'data-click' => 'deleteMedia',
						'data-locale' => 'false',
						'class' => 'delete media left'
					)
				)
			)
		);
		*/

		public $aScripts = [
			'public/modules/media/cms/js/chani/Media.js'
		];

		public function onConstruct() {
			$aNameSpace = explode('\\',__NAMESPACE__);

			$this->view = new Simple();
			$this->view->registerEngines(array(
				".volt" => 'Phalcon\Mvc\View\Engine\Volt'
			));

			$this->view->setViewsDir(MODULE_DIR.'/'.strtolower($aNameSpace[0]).'/views/');

			$oDi = new FactoryDefault();
			$oDi->set('url', function() {
				$url = new \Phalcon\Mvc\Url();
				$url->setBaseUri('/');
				return $url;
			}, true);
			$this->view->setDI($oDi);
		}

		public function indexAction() {
			echo('This is the '.get_class($this));
		}

		/**
		 * Processes Youtube and Vimeo URLs posted by the user
		 */
		public function processVideoAction() {
			$iModelId = $this->request->getPost('id',null,0);
			$sModel = $this->request->getPost('model',null,'');

			$sUrl = $this->request->getPost('sUrl',null,'');
			if($sUrl != '') {
				$sJSON = $this->processVideo($sUrl, $sModel, $iModelId);
				echo($sJSON);
			}
		}

		/**
		 * @param $sUrl
		 * @param $sModel
		 * @param $iModelId
		 * @return string
		 * Uses the Vimeo / Youtube APIs to retrieve video information and thumbnails
		 */
		public function processVideo($sUrl, $sModel, $iModelId) {
			$aVideoRegEx = array(
				'youtube' => array(
					'/youtu[\.]be\/(.{11})[\/]?/',
					'/(?:www[\.])?youtube[\.]com\/watch[\?](?:.*)v=(.{11})(?:.*)/'
				),
				'vimeo' => array(
					'/vimeo[\.]com\/(\d+)[\/]?/'
				)
			);
			$aMatches = array();
			$sVideoId = '';
			$oJSON = null;
			for($i = 0; $i < count($aVideoRegEx['youtube']); $i++) {
				if(preg_match($aVideoRegEx['youtube'][$i],$sUrl,$aMatches) == 1) {
					$sType = 'youtube';
					$sVideoId = $aMatches[1];
					$sApiUrl = 'https://www.googleapis.com/youtube/v3/videos?part=snippet&id='.$sVideoId.'&key=AIzaSyCgveNoarPO_pfyrqG2gmWeEholwJoENZ4';
					// Get cURL resource
					$curl = curl_init();
					// Set some options - we are passing in a useragent too here
					curl_setopt_array($curl, array(
						CURLOPT_RETURNTRANSFER => 1,
						CURLOPT_URL => $sApiUrl,
						CURLOPT_USERAGENT => 'Chani CMS'
					));
					// Send the request & save response to $resp
					$sJSON = curl_exec($curl);
					// Close request to clear up some resources
					curl_close($curl);
					if($sJSON !== false) {
						$oJSON = json_decode($sJSON);
						$sEmbed = '';
					}
					break;
				}
			}

			if($sVideoId == '') {
				for($i = 0; $i < count($aVideoRegEx['vimeo']); $i++) {
					if(preg_match($aVideoRegEx['vimeo'][$i],$sUrl,$aMatches) == 1) {
						$sVideoId = $aMatches[1];
						$sType = 'vimeo';
						$oVimeoController = new VimeoController();
						$oJSON = $oVimeoController->getVimeoData($sVideoId);
						$sEmbed = $oJSON['body']['embed']['html'];
						break;
					}
				}
			}

			$aFiles = array();
			if($oJSON !== null) {
				$oMedia = CmsMedia::add($sModel, $iModelId, null);
				if($oMedia !== false) {
					$oMedia->sEmbed = $sEmbed;
					$oMedia->sUrl = $sUrl;
					$oMedia->sVideoId = $sVideoId;
					$oMedia->sExtension = 'jpg';
					$oMedia->saveData();

					$sPath = $oMedia->getPath('original');
					if($this->grabThumbNail($oJSON,$sType, $sPath)) {
						$oMedia->resize(100, 0,0,0,0, 'original');
						$oMedia->resize(100, 0, 0, 0, 0, 'cmsthumbnail', 142, 142, true);
						$oMedia->resize(100, 0,0,0,0, 'view');
						$oMedia->resize(100, 0,0,0,0, 'crop');
					}
					$aFiles = array();
					$aFiles[] = $this->process($oMedia);
				} else {
					echo('media not found');
					die;
				}

			}
			return json_encode($aFiles);
		}

		/**
		 * @param $oJSON
		 * @param $sType
		 * @param $sPath
		 * @return bool
		 * Retrieve thumbnail from Vimeo / Youtube response
		 */
		public function grabThumbNail($oJSON, $sType, $sPath) {
			if($sType == 'youtube') {
				/*
				 *
					Youtube

					->items[0]->snippet->thumbnails->default 120 x 90
					->items[0]->snippet->thumbnails->medium 320 x 180
					->items[0]->snippet->thumbnails->high 480 x 360
					->items[0]->snippet->thumbnails->standard 640 x 480
					->items[0]->snippet->thumbnails->maxres 1280 x 720
					->url
					->width
					->height

					->items[0]->snippet->title
					->items[0]->snippet->description
	            */
				$sUrl = $oJSON->items[0]->snippet->thumbnails->high->url;

			} elseif($sType == 'vimeo') {
				/*
				 *  ['body']['name']
					['body']['description']
					['body']['pictures']['sizes'][0]['link']
					[0] 100 x 75
					[1] 200 x 150
					[2] 295 x 166
					[3] 640 x 360
					[4] 960 x 540
					[5] 1280 x 720
				 */
				$sUrl = $oJSON['body']['pictures']['sizes'][3]['link'];
			} else {
				return false;
			}

			file_put_contents($sPath, file_get_contents($sUrl));
			return true;
		}

		/**
		 * Handles the files a user has uploaded
		 */
		public function processUploadAction() {

			//Check if the user has uploaded files
			if ($this->request->hasFiles() == true) {

				//Print the real file names and their sizes
				$i = 0;
				$iModelId = $this->request->getPost('id',null,0);
				$sModel = $this->request->getPost('model',null,'');
				$iLocaleId = $this->request->getPost('iLocaleId',null,116);

				$aFiles = array();
				if($iModelId > 0 && $sModel != '') {
					foreach ($this->request->getUploadedFiles() as $oFile) {
						$i++;
						$oMedia = CmsMedia::add($sModel, $iModelId, $oFile);
						$aFiles[] = $this->process($oMedia, $iLocaleId);
					}
				}
				echo json_encode($aFiles);

			} else {

				echo('no files were uploaded');
				die;

			}
		}

		/**
		 * Saves an altered value to the media tables
		 */
		public function saveValueAction() {

			$aPost = $this->request->getPost();

			$id = $aPost['id'];
			$sValue = $aPost['sValue'];
			$sColumn = $aPost['sColumn'];
			$iLocaleId = $aPost['iLocaleId'];
			$bLocale = $aPost['bLocale'];

			$oMedia = CmsMedia::findFirst($id);

			if($bLocale === 'true') {

				$oEditable = $oMedia->getLocales(array(
					'iLocaleId = :iLocaleId:',
					'bind' => array(
						'iLocaleId' => $iLocaleId
					)
				));

				$oEditable[0]->$sColumn = $sValue;

				if($oEditable[0]->$sColumn.'Url') {

				}

				if($oEditable[0]->saveData() == false) {

					$oMessages = $oEditable[0]->getMessages();

					foreach($oMessages AS $oMessage) {

						echo($oMessage->getMessage().'<br />');

					}

					die;

				}

			} else {

				$oMedia->$sColumn = $sValue;
				$oMedia->saveData();

			}

			echo('success');

		}

		/**
		 * Saves and creates cropped versions of an image
		 */
		public function saveCropAction() {

			$iMediaId = $this->request->getPost('iMediaId',null,0);
			$iScale = $this->request->getPost('scale',null,100);
			$iCropX = $this->request->getPost('cropX',null,0);
			$iCropY = $this->request->getPost('cropY',null,0);
			$iCropWidth = $this->request->getPost('cropWidth',null,0);
			$iCropHeight = $this->request->getPost('cropHeight',null,0);

			if($iMediaId > 0) {

				$oMedia = CmsMedia::findFirst($iMediaId);
				$oMedia->resize($iScale, $iCropX, $iCropWidth, $iCropY, $iCropHeight, 'crop');
				$oMedia->resize(100, $iCropX, $iCropWidth, $iCropY, $iCropHeight, 'cmsthumbnail', 142, 142, true);
				$aUrl = $oMedia->getUrl('crop');
				echo($this->url->get($aUrl['src']));

			}

		}

		/**
		 * @param $oMedia
		 * @return bool|\Phalcon\Mvc\View
		 * Prepares the info and renders the HTML necessary for editing Media in the dashboard
		 */
		public function process($oMedia, $iLocaleId = null) {
			$aImg = $oMedia->getUrl('crop', false, false);
			$oCrop = $oMedia->getCrops(array(
				'conditions' => 'sType = :sType:',
				'bind' => array(
					'sType' => 'crop'
				)
			));

			if($oCrop->count() == 0) {
				echo('crop not found');
				die;
			} else {
				$oCrop = $oCrop[0];
			}

			if($oMedia->isVideo() == true) {
				$bVideo = true;
				$aImg['class'] = 'video';
			} else {
				$bVideo = false;
			}

			$aOriginalUrl = $oMedia->getUrl('original');
			$aMedia = array(
				'id' => $oMedia->id,
				'fileName' => $oMedia->sFileName,
				'originalUrl' => $aOriginalUrl['src']
			);
			$aImg['data-model'] = $oMedia->sModel;
			$aImg['data-model-id'] = $oMedia->iModelId;
			$aImg['id'] = 'media_'.$oMedia->id;
			$aImg['data-target'] = 'image';
			$aImg['data-id'] = $oMedia->id;
			$aImg['data-url'] = $this->url->get(str_replace('crop','original',$aImg['src']));
			$aImg['data-scale'] = $oCrop->iWidthPercentage;
			$aImg['data-crop-x'] = $oCrop->iCropX;
			$aImg['data-crop-y'] = $oCrop->iCropY;
			$aImg['data-crop-width'] = $oCrop->iCropWidth;
			$aImg['data-crop-height'] = $oCrop->iCropHeight;
			$aImg['style'] = 'max-width: 100%; height: auto;';
			if($bVideo) {
				$type = 'video';
				$sEmbed = $oMedia->sEmbed;
				$iWidth = $oMedia->iVideoWidth;
				$iHeight = $oMedia->iVideoHeight;
				if($sEmbed != '' && 2 == 3) {
					$sEmbed = str_replace('<iframe ', '<iframe id="mediaIframe'.$oMedia->id.'" ', $sEmbed);
					$sEmbed = preg_replace('/width="\d+"/','width="'.$iWidth.'"',$sEmbed);
					$sEmbed = preg_replace('/height="\d+"/','height="'.$iHeight.'"',$sEmbed);
					$aMedia['img'] = $sEmbed;
				} else {
					$aMedia['img'] = Tag::image($aImg);
				}

				$aMedia['url'] = $oMedia->sUrl;
				$aMedia['iWidth'] = $iWidth;
				$aMedia['iHeight'] = $iHeight;

			} else {
				$type = 'image';
				$aMedia['img'] = Tag::image($aImg);
				$aMedia['title'] = '';
				$aMedia['alt'] = '';
				if($iLocaleId !== null) {
					$oLocale = $oMedia->getLocales([
						'conditions' => 'iLocaleId = :iLocaleId:',
						'bind' => [
							'iLocaleId' => $iLocaleId
						]
					]);
					if(count($oLocale) > 0) {
						$aMedia['title'] = $oLocale[0]->sTitle;
						$aMedia['alt'] = $oLocale[0]->sAlt;
					}
				}
			}
			$aMedia['type'] = $type;
			$sMedia = $this->view->render('chani/media',array(
				'media' => $aMedia,
				'bVideo' => $bVideo
			));
			return $sMedia;
		}

		/**
		 * Flags media for deletion from the database
		 */
		public function deleteMediaAction() {
			$id = $this->request->getPost('id',null,0);

			if($id > 0) {
				$oMedia = CmsMedia::findFirst($id);
				if($oMedia !== false) {
					$oMedia->bDeleted = 1;
					if($oMedia->saveData() !== false) {
						echo('success');
					} else {
						echo('error deleting media');
					}
					die;
				} else {
					echo('record not found');
					die;
				}
			}
			echo('error: no id given');
			die;
		}

		/**
		 * @param $sModel
		 * @param $iModelId
		 * @return bool|\Phalcon\Mvc\View
		 * Generates and returns the HTML for the media upload form
		 */
		public function getUploadView($sModel, $iModelId) {
			$sScript = Tag::javascriptInclude('public/modules/media/cms/js/chani/html5upload.js');

			$sFormId = $sModel.'_'.$iModelId;

			$sUrl = $this->url->get('chani/media/processUpload');
			$sVideoUrl = $this->url->get('chani/media/processVideoUrl');

			$sView = $this->view->render('chani/html5upload',array(
				'iModelId' => $iModelId,
				'script' => $sScript,
				'sFormId' => $sFormId,
				'url' => $sUrl,
				'videoUrl' => $sVideoUrl,
				't' => $this->_getTranslation(__NAMESPACE__)
			));

			return $sView;
		}

		/**
		 * @return string
		 * Return the Javascript that is to be run on page load for this module
		 */
		public function getEditDomready() {
			$sDomready = '
			oMedia = new chaniMedia({
				className: \'media\',
				id: 0,
				iLocaleId: 116,
				ajax: {
					saveValueUrl: \''.$this->url->get('cms/media/saveValue').'\',
					deleteMediaUrl: \''.$this->url->get('cms/media/deleteMedia').'\',
					mediaSettingsUrl: \''.$this->url->get('cms/media/getMediaSettings').'\'
				}
			});';
			return $sDomready;
		}

		public function getDashboardModals($aModelInfo = array()) {
			$t = $this->_getTranslation(__NAMESPACE__);
			$dashboardModals = [
				'media' => ['html' => $this->view->render('chani/dashboard-media',['t' => $t]), 'buttons' => ''],
				'image' => ['html' => $this->view->render('chani/dashboard-image',['t' => $t]), 'buttons' => ''],
				'video' => ['html' => $this->view->render('chani/dashboard-video',['t' => $t]), 'buttons' => '']
			];
			return $dashboardModals;
		}

		/**
		 * @return \Phalcon\Translate\Adapter\NativeArray
		 * Gets the translations for the language the user is currently using
		 * TODO: Tweak this function to handle non-existing translation locales and force it to use 'nl' instead of 'nl-NL'
		 */
		public function _getTranslation($sNameSpace = '')
		{

			//Determine the browser language of the user
			$sLanguage = $this->request->getBestLanguage();
			//Format returned for example: en-EN - we only need the first part
			$aLanguage = explode('-',$sLanguage);
			$sLanguage = $aLanguage[0];

			//If no namespace is provided, use the current one
			if($sNameSpace == '') {
				$sNameSpace = __NAMESPACE__;
			}

			//Build the regular path, the chani path and fallback (english) path using the namespace,
			$aNameSpace = explode('\\',$sNameSpace);
			$sPath = MODULE_DIR.'/'.strtolower($aNameSpace[0]).'/translations/';
			$sChaniPath = $sPath.'chani/';
			$sFallback = $sChaniPath.'en.php';
			$sPath .= $sLanguage.'.php';
			$sChaniPath .= $sLanguage.'.php';

			//Check if we have a translation file for that language, if not fall back to whatever is available
			$aMessages = null;
			if (file_exists($sPath)) {
				require $sPath;
			} elseif(file_exists($sChaniPath)) {
				// fallback to some default
				require $sChaniPath;
			} else {
				require $sFallback;
			}

			//Return a translation object
			return new Translator(array("content" => $aMessages));

		}

	}