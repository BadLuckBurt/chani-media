<?php

	$aMessages = array(
		'module' => 'Media',
		'sTitle'    =>'Titel',
		'sAlt'      => 'Alt tekst',
		'sContent'  => 'Omschrijving',
		'sUrl'      => 'Video url',
		'save'      => 'Opslaan',
		'processUrl'      => 'Verwerk URL',
		'addFile' => 'Voeg bestand toe',
		'uploadFiles'=> 'Upload bestanden',
		'delete' => 'Verwijder media',
		'cropper' => 'Open cropper'
	);