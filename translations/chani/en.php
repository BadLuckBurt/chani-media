<?php

	$aMessages = array(
		'module' => 'Media',
		'sTitle'    =>'Titel',
		'sAlt'      => 'Alt text',
		'sContent'  => 'Description',
		'sUrl'      => 'Video url',
		'save'      => 'Save',
		'processUrl'      => 'Process URL',
		'addFile' => 'Add file',
		'uploadFiles' => 'Upload files',
		'delete' => 'Delete media',
		'cropper' => 'Open cropper'
	);