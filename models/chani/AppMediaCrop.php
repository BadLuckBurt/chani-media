<?php

	namespace Media\Models\Chani;
	use \Core\Models\Chani\AppBlueprint,
	\Core\Shared;

	class AppMediaCrop extends AppBlueprint {

		public $dtCreated;
		public $dtUpdated;
		public $iModelId;
		public $iWidthPercentage;
		public $iWidth;
		public $iHeight;
		public $iCropX;
		public $iCropWidth;
		public $iCropY;
		public $iCropHeight;
		public $sType;
		public $_model ='AppMediaCrop';

		public function getSource() {
			return 'mediacrop';
		}

		public function initialize() {

			$sClass = $this->_model;
			$sClass = $this->testClass(__NAMESPACE__,$sClass);
			$this->hasOne('iModelId',$sClass,'id', array('alias' => $sClass));

			parent::initialize();
		}

	}