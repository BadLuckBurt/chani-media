<?php

	namespace Media\Models\Chani;

	use \Core\Models\Chani\AppBlueprint,
	\Core\Shared;

	class AppMediaLocale extends AppBlueprint {

		public $_model ='AppMediaLocale';

		public function getSource() {
			return 'medialocale';
		}

		public function initialize() {

			$sClass = $this->_model;
			$sClass = $this->testClass(__NAMESPACE__,$sClass);
			$this->hasOne('iModelId',$sClass,'id', array('alias' => $sClass));

			parent::initialize();
		}

	}