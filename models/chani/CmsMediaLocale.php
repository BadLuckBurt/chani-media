<?php

	namespace Media\Models\Chani;

	use \Core\Models\Chani\CmsBlueprint,
		\Core\Shared;

	class CmsMediaLocale extends CmsBlueprint {

		public $_model ='CmsMediaLocale';

		public function getSource() {
			return 'medialocale';
		}

		public function initialize() {

			$sClass = str_replace('Locale','',$this->_model);
			$sClass = $this->testClass(__NAMESPACE__,$sClass);
			$this->hasOne('iModelId',$sClass,'id', array('alias' => $sClass));

			parent::initialize();
		}

		/**
		 * @param null $aData
		 * @param null $aWhiteList
		 * @return bool
		 * Save data, allowing empty strings to be saved
		 */
		public function save($aData = null, $aWhiteList = null) {

			if($this->sTitle == '' || $this->sTitle === null) {
				$this->sTitle = new \Phalcon\Db\RawValue('""');
			}
			if($this->sAlt == '' || $this->sAlt === null) {
				$this->sAlt = new \Phalcon\Db\RawValue('""');
			}
			if($this->sContent == '' || $this->sContent === null) {
				$this->sContent = new \Phalcon\Db\RawValue('""');
			}
			return parent::save($aData, $aWhiteList);
		}

		/**
		 * @param $iModelId
		 * Creates an editable version of an existing record
		 */
		public function makeEditable($iModelId) {
			$oEditable = static::add($iModelId, $this->iLocaleId, $this->sTitle, $this->sAlt, $this->sContent);
		}

		/**
		 * @param $iModelId
		 * @param $iLocaleId
		 * @param string $sTitle
		 * @param string $sAlt
		 * @param string $sContent
		 * @return CmsMediaLocale
		 * Create a new MediaLocale record and returns the result
		 */
		public static function add($iModelId, $iLocaleId, $sTitle = '', $sAlt = '', $sContent = '') {
			$oLocale = new CmsMediaLocale();

			$oLocale->dtCreated = Shared::getDBDate();
			$oLocale->dtUpdated = 0;
			$oLocale->iModelId = $iModelId;
			$oLocale->iLocaleId = $iLocaleId;

			if($sTitle == '') {
				$oLocale->sTitle = new \Phalcon\Db\RawValue('""');
			} else {
				$oLocale->sTitle = $sTitle;
			}

			if($sAlt == '') {
				$oLocale->sAlt = new \Phalcon\Db\RawValue('""');
			} else {
				$oLocale->sAlt = $sAlt;
			}

			if($sContent == '') {
				$oLocale->sContent = new \Phalcon\Db\RawValue('""');
			} else {
				$oLocale->sContent = $sContent;
			}

			if($oLocale->create() == false) {
				$oMessages = $oLocale->getMessages();

				foreach($oMessages AS $oMessage) {
					echo $oMessage->getMessage();
				}
				die;
			}
			return $oLocale;
		}

	}