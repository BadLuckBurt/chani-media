<?php

	namespace Media\Models\Chani;
	use \Core\Models\Chani\CmsBlueprint,
		\Core\Shared;

	class CmsMediaCrop extends CmsBlueprint {

		public $dtCreated;
		public $dtUpdated;
		public $iModelId;
		public $iWidthPercentage;
		public $iWidth;
		public $iHeight;
		public $iCropX;
		public $iCropWidth;
		public $iCropY;
		public $iCropHeight;
		public $sType;
		public $_model ='CmsMediaCrop';

		public function getSource() {
			return 'mediacrop';
		}

		public function initialize() {

			$sClass = str_replace('Crop','',$this->_model);
			$sClass = $this->testClass(__NAMESPACE__,$sClass);

			$this->hasOne('iModelId',$sClass,'id', array('alias' => $sClass));
			parent::initialize();
		}

		/**
		 * @param $iModelId
		 * @param int $iWidthPercentage
		 * @param int $iWidth
		 * @param int $iHeight
		 * @param int $iCropX
		 * @param int $iCropWidth
		 * @param int $iCropY
		 * @param int $iCropHeight
		 * @param string $sType
		 * @return CmsMediaCrop|\Phalcon\Mvc\Model
		 * Creates a new MediaCrop record and returns the object
		 */
		public static function add($iModelId, $iWidthPercentage = 100, $iWidth = 0, $iHeight = 0, $iCropX = 0, $iCropWidth = 0, $iCropY = 0, $iCropHeight = 0, $sType = 'crop') {

			$oMediaCrop = static::findFirst(array(
				'conditions' => 'iModelId = :iModelId: AND sType = :sType:',
				'bind' => array(
					'iModelId' => $iModelId,
					'sType' => $sType
				)
			));

			if($oMediaCrop == false) {

				$oMediaCrop = new CmsMediaCrop();
				$oMediaCrop->dtCreated = Shared::getDBDate();
				$oMediaCrop->iModelId = $iModelId;

			} else {

				$oMediaCrop->dtUpdated = Shared::getDBDate();

			}

			$oMediaCrop->iWidthPercentage = $iWidthPercentage;
			$oMediaCrop->iWidth = $iWidth;
			$oMediaCrop->iHeight = $iHeight;
			$oMediaCrop->iCropX = $iCropX;
			$oMediaCrop->iCropWidth = $iCropWidth;
			$oMediaCrop->iCropY = $iCropY;
			$oMediaCrop->iCropHeight = $iCropHeight;
			$oMediaCrop->sType = $sType;

			if($oMediaCrop->saveData() == false) {

				foreach($oMediaCrop->getMessages() AS $message) {

					echo($message.'<br />');

				}

				die;

			} else {

			}

			return $oMediaCrop;

		}

	}