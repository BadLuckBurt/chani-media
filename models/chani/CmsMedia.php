<?php

	namespace Media\Models\Chani;

	use \Core\Models\Chani\CmsBlueprint,
		\Core\Shared,
		\Phalcon\Image\Adapter\GD,
		\Phalcon\Text,
		\Phalcon\Db\Column,
		\L10n\Models\Chani\CmsL10n;

	class CmsMedia extends CmsBlueprint {

		public $iSourceId;
		public $iOriginalId;
		public $dtCreated;
		public $dtUpdated;
		public $sFileName;
		public $sExtension;
		public $iSizeBytes;
		public $sModel;
		public $iModelId;
		public $sSessionId;
		public $sUrl;
		public $sVideoId;
		public $sEmbed;
		public $iSequence;
		public $bDeleted;
		public $iVideoWidth;
		public $iVideoHeight;
		public $_model ='CmsMedia';

		public function getSource() {
			return 'media';
		}

		public function initialize() {

			parent::initialize();

			$sLocaleClass = $this->_model.'Locale';
			$sLocaleClass = $this->testClass(__NAMESPACE__,$sLocaleClass);

			$this->hasMany('id', $sLocaleClass, 'iModelId',array('alias' => 'Locales'));

			$sCropClass = $this->_model.'Crop';
			$sCropClass = $this->testClass(__NAMESPACE__,$sCropClass);

			$this->hasMany('id', $sCropClass, 'iModelId',array('alias' => 'Crops'));

		}

		/**
		 * @param $aConditions
		 * @return \Phalcon\Mvc\Model\ResultsetInterface
		 * Returns all media crops that satisfy the supplied conditions
		 */
		public function getCrops($aConditions) {
			$oCrops = $this->getRelated('Crops',$aConditions);
			return $oCrops;
		}

		/**
		 * @return bool
		 * Deletes a media record, it's locales and it's crop versions
		 */
		public function deleteMedia() {

			$this->clearCrops();
			$oLocales = $this->getLocales();
			for($i = 0; $i < count($oLocales); $i++) {
				$oLocales[$i]->delete();
			}
			return parent::delete();
		}

		/**
		 * @param array $aIgnore
		 * Clears all crops except the type specified in ignore: default is 'view'
		 */
		public function clearCrops($aIgnore = array('view')) {
			$oCrops = $this->getCrops(array(
				'conditions' => "sType IN ('original','cmsthumbnail','view','crop')"
			));

			for($i = 0; $i < count($oCrops); $i++) {

				if(!in_array($oCrops[$i]->sType,$aIgnore)) {
					$sPath = $this->getPath($oCrops[$i]->sType);
					if(file_exists($sPath)) {
						unlink($sPath);
					}
					$oCrops[$i]->delete();
				}
			}
		}

		/**
		 * @param bool $bImage
		 * @return string
		 * Returns the path of the original image
		 */
		public function getOriginalPath($bImage = false) {
			$sExtension = $this->sExtension;
			$sModel = $this->sModel;
			return BASE_DIR.'/public/modules/'.$sModel.'/media/original/'.$this->id.'.'.$sExtension;
		}

		/**
		 * @param $sType
		 * @return string
		 * Returns a path to an image based on crop type
		 */
		public function getPath($sType) {
			$sModel = $this->sModel;
			if($this->iOriginalId > 0 && $sType == 'view') {
				$id = $this->iOriginalId;
			} else {
				$id = $this->id;
			}
			return BASE_DIR.'/public/modules/'.$sModel.'/media/'.$sType.'/'.$id.'.'.$this->sExtension;
		}

		/**
		 * @param string $sType
		 * @param bool $bWidth
		 * @param bool $bHeight
		 * @return array
		 * Returns an array with the properties to generate an <img> tag
		 */
		public function getUrl($sType = 'view', $bWidth = false, $bHeight = false) {

			$sModel = $this->sModel;

			$oMediaCrop = CmsMediaCrop::findFirst(array(
				'conditions' => 'sType = :sType:',
				'bind' => array(
					'sType' => $sType
				)
			));

			if($oMediaCrop === false) {
				echo('not found');
				die;
			} else {
				if($this->iOriginalId > 0 && $sType == 'view') {
					$id = $this->iOriginalId;
				} else {
					$id = $this->id;
				}
				$sDate = $oMediaCrop->dtUpdated;
				$aInfo = array(
					'id' => $id,
					'src' => strtolower('public/modules/'.$sModel.'/media/'.$sType.'/'.$id.'.'.$this->sExtension.'?date='.$sDate)
				);

				if($bWidth == true) {
					$aInfo['width'] = $oMediaCrop->iCropWidth;
				}

				if($bHeight == true) {
					$aInfo['height'] = $oMediaCrop->iCropHeight;
				}

				return $aInfo;
			}
		}

		public function getSequence() {

			$SQL = 'SELECT COUNT(id) + 1 AS counter
					FROM '.get_class($this).'
					WHERE iModelId = :iModelId:';

			$oQuery = new \Phalcon\Mvc\Model\Query($SQL, $this->getDI());
			$oQuery->setBindParams(array(
				'iModelId'  => $this->iModelId
			));

			$oCount = $oQuery->execute();

			return $oCount[0]->counter;

		}

		/**
		 * @param $iModelId
		 * Copy media for editing, preserving the 'live' version
		 */
		public function makeEditable($iModelId) {

			$oEditable = new CmsMedia();
			$oEditable->iSourceId = $this->id;
			if($this->iOriginalId == 0) {
				$oEditable->iOriginalId = $this->id;
			} else {
				$oEditable->iOriginalId = $this->iOriginalId;
			}
			$oEditable->dtCreated = Shared::getDBDate();
			$oEditable->dtUpdated = 0;
			$oEditable->sFileName = $this->sFileName;
			$oEditable->sExtension = $this->sExtension;
			$oEditable->iSizeBytes = $this->iSizeBytes;
			$oEditable->sModel = $this->sModel;
			$oEditable->iModelId = $iModelId;
			$oEditable->sSessionId = $this->sSessionId;
			$oEditable->sUrl = $this->sUrl;
			$oEditable->sVideoId = $this->sVideoId;
			$oEditable->sEmbed = $this->sEmbed;
			$oEditable->iSequence = $this->iSequence;
			$oEditable->bDeleted = 0;
			$oEditable->iVideoWidth = $this->iVideoWidth;
			$oEditable->iVideoHeight = $this->iVideoHeight;

			if($oEditable->saveData() == false) {

				foreach($oEditable->getMessages() AS $message) {

					echo($message.'<br />');

				}
				die;

			}

			$sFilePath = $this->getPath('original');
			$sNewPath = $oEditable->getPath('original');
			copy($sFilePath,$sNewPath);

			$oMediaCrop = $this->getCrops(array(
				'conditions' => "sType IN ('original','cmsthumbnail','view','crop')"
			));

			foreach($oMediaCrop AS $oCrop) {
				CmsMediaCrop::add($oEditable->id, $oCrop->iWidthPercentage, $oCrop->iWidth, $oCrop->iHeight, $oCrop->iCropX, $oCrop->iCropWidth, $oCrop->iCropY, $oCrop->iCropHeight, $oCrop->sType);
			}
			$sFilePath = $this->getPath('view');
			$sNewPath = $oEditable->getPath('crop');
			copy($sFilePath,$sNewPath);

			$sFilePath = $this->getPath('cmsthumbnail');
			$sNewPath = $oEditable->getPath('cmsthumbnail');
			copy($sFilePath,$sNewPath);

			$oLocales = $this->getLocales();
			for($i = 0; $i < count($oLocales); $i++) {
				$oLocales[$i]->makeEditable($oEditable->id);
			}

		}

		/**
		 * @param $sModel
		 * @param $iModelId
		 * @param null $oFile
		 * @return CmsMedia
		 * Creates a new Media record
		 */
		public static function add($sModel, $iModelId, $oFile = null) {

			if($oFile !== null) {
				//Retrieve the filename and extension
				$sFileName = $oFile->getName();
				$aFilename = explode('.',$sFileName);
				$sExtension = end($aFilename);
			} else {
				$sFileName = '';
				$sExtension = '';
			}

			//This means the file has no extension
			if($sExtension == $sFileName) {
				$sExtension = '';
			}

			$bImage = false;
			//Check for type by extension
			switch($sExtension) {
				case 'bmp':
					$sExtension = 'jpg';
				case 'gif':
				case 'jpg':
				case 'png':
					$bImage = true;
					break;
				case 'flv':

				case 'swf':

				default:

			}

			//Prepare and save the record
			$oMedia = new CmsMedia();
			$oMedia->iSourceId = 0;
			$oMedia->iOriginalId = 0;
			$oMedia->dtCreated = Shared::getDBDate();
			$oMedia->dtUpdated = 0;
			$oMedia->iModelId = $iModelId;
			$oMedia->sSessionId = '';
			$oMedia->sUrl = '';
			$oMedia->sVideoId = '';
			$oMedia->sEmbed = '';
			$oMedia->sModel = $sModel;
			$oMedia->sFileName = $sFileName;
			$oMedia->sExtension = $sExtension;
			$oMedia->iSequence = $oMedia->getSequence();
			$oMedia->bDeleted = 0;
			$oMedia->iVideoWidth = 38 * 16;;
			$oMedia->iVideoHeight = 20 * 16;
			$oMedia->iSizeBytes = 0;

			if($oMedia->saveData() == false) {

				foreach($oMedia->getMessages() AS $message) {

					echo($message.'<br />');

				}

				die;

			} else {

				if($oFile !== null) {
					//Move the uploaded file to the proper location
					$sOriginalPath = $oMedia->getOriginalPath();
					$oFile->moveTo($sOriginalPath);
				}
				$oMedia->createLocales();

				if($bImage == true) {

					//Create an original version
					$oMedia->resize(100, 0,0,0,0, 'original');
					$oMedia->resize(100, 0, 0, 0, 0, 'cmsthumbnail', 142, 142, true);
					$oMedia->resize(100, 0,0,0,0, 'view');
					$oMedia->resize(100, 0,0,0,0, 'crop');

				} else {

					//Copy file

				}
			}

			return $oMedia;

		}

		/**
		 * Creates the locale records for media
		 */
		public function createLocales() {
			$oL10n = new CmsL10n();
			$oLanguages = $oL10n->getInputLanguages();
			for($i = 0; $i < count($oLanguages); $i++) {
				CmsMediaLocale::add($this->id, $oLanguages[$i]->id);
			}
		}

		/**
		 * @param int $iWidthPercentage
		 * @param int $iCropX
		 * @param int $iCropWidth
		 * @param int $iCropY
		 * @param int $iCropHeight
		 * @param string $sType
		 * @param null $iImageWidth
		 * @param null $iImageHeight
		 * @param bool $bCropToSize
		 * @return CmsMediaCrop|\Phalcon\Mvc\Model
		 * Perform a resize / crop operation on an image
		 */
		public function resize($iWidthPercentage = 100, $iCropX = 0, $iCropWidth = 0, $iCropY = 0, $iCropHeight = 0, $sType = 'crop', $iImageWidth = null, $iImageHeight = null, $bCropToSize = false) {

			$bSkip = 0;

			$sOriginalPath = $this->getOriginalPath();
			$sPath = $this->getPath($sType);

			$oImage = new GD($sOriginalPath);
			$iWidth = $oImage->getWidth();
			$iHeight = $oImage->getHeight();

			//When both the width and height are the full image, add 1 to skip
			if($iCropX == 0 && $iCropWidth == 0) {

				$iCropWidth = $iWidth;
				$bSkip++;

			}

			if($iCropY == 0 && $iCropHeight == 0) {

				$iCropHeight = $iHeight;
				$bSkip++;

			}

			$iCropX = (int) $iCropX;
			$iCropWidth = (int) $iCropWidth;
			$iCropY = (int) $iCropY;
			$iCropHeight = (int) $iCropHeight;


			if($bSkip < 2) {
				$iCropX = max($iCropX, 0);
				$iCropY = max($iCropY, 0);

				$oImage->crop($iCropWidth, $iCropHeight,$iCropX, $iCropY);
			}

			if($iWidthPercentage < 100) {
				//Get the original width and height of the image
				$iWidth = $oImage->getWidth();
				$iHeight = $oImage->getHeight();

				//Calculate the new width
				$iNewWidth = ceil($iWidth * ($iWidthPercentage / 100));

				//Calculate the ratio to the original and new size and scale the height accordingly
				$fRatio = $iNewWidth / $iWidth;
				$iNewHeight = floor($iHeight * $fRatio);

				$oImage->resize($iNewWidth, $iNewHeight);

			} elseif($iImageWidth > 0 || $iImageHeight > 0) {

				if($iImageWidth > 0 && $iImageHeight > 0) {
					$oImage->resize($iImageWidth, $iImageHeight, \Phalcon\Image::PRECISE);
				} elseif($iImageWidth > 0) {
					$oImage->resize($iImageWidth);
				} else {
					$oImage->resize(null, $iImageHeight);
				}

				if(2 == 3) {

					if($oImage->getWidth() < $iImageWidth || $oImage->getHeight() < $iImageHeight) {

						if($oImage->getWidth() < $iImageWidth) {
							$oImage->resize($iImageWidth);
						}
						if($oImage->getHeight() < $iImageHeight) {
							$oImage->resize(null, $iImageHeight);
						}
					}
				}

				if($bCropToSize == true) {
					$iOffsetX = ($oImage->getWidth() - $iImageWidth) / 2;
					$iOffsetY = ($oImage->getHeight() - $iImageHeight) / 2;
					//$oImage->crop($iImageWidth, $iImageHeight, $iOffsetX, $iOffsetY);
				}

			}

			$iWidth = $oImage->getWidth();
			$iHeight = $oImage->getHeight();

			$oImage->save($sPath, 100);
			//Save the settings to the database
			return CmsMediaCrop::add($this->id, $iWidthPercentage, $iWidth, $iHeight, $iCropX, $iCropWidth, $iCropY, $iCropHeight, $sType);

		}

		/**
		 * @return bool
		 * Save the cropped version of an image
		 */
		public function saveCrop() {
			$sFilePath = $this->getPath('crop');
			$sNewPath = $this->getPath('view');
			copy($sFilePath, $sNewPath);
			return true;
		}

		/**
		 * @return bool
		 * Check if the media is video
		 */
		public function isVideo() {
			if($this->sUrl != '' && $this->sVideoId !== '') {
				return true;
			} else {
				return false;
			}
		}

	}