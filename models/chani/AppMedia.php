<?php

	namespace Media\Models\Chani;

	use \Core\Models\Chani\AppBlueprint,
	\Core\Shared,
	\Phalcon\Image\Adapter\GD,
	\Phalcon\Text,
	\Phalcon\Db\Column;

	class AppMedia extends AppBlueprint {

		public $iSourceId;
		public $iOriginalId;
		public $dtCreated;
		public $dtUpdated;
		public $sFileName;
		public $sExtension;
		public $iSizeBytes;
		public $sModel;
		public $iModelId;
		public $sSessionId;
		public $sUrl;
		public $sVideoId;
		public $sEmbed;
		public $iSequence;
		public $bDeleted;
		public $iVideoWidth;
		public $iVideoHeight;
		public $_model ='AppMedia';

		public function getSource() {
			return 'media';
		}

		public function initialize() {

			parent::initialize();

			$sLocaleClass = $this->_model.'Locale';
			$sLocaleClass = $this->testClass(__NAMESPACE__,$sLocaleClass);

			$this->hasMany('id', $sLocaleClass, 'iModelId',array('alias' => 'locales'));

			$sCropClass = $this->_model.'Crop';
			$sCropClass = $this->testClass(__NAMESPACE__,$sCropClass);

			$this->hasMany('id', $sCropClass, 'iModelId',array('alias' => 'crops'));

		}

		/**
		 * @param string $sType
		 * @param null $iLocaleId
		 * @param bool $bWidth
		 * @param bool $bHeight
		 * @return array
		 * Returns the properties needed to render an image tag for media
		 */
		public function getUrl($sType = 'view', $iLocaleId = null, $bWidth = false, $bHeight = false) {

			$sModel = $this->sModel;

			$oMediaCrop = AppMediaCrop::findFirst(array(
				'conditions' => 'sType = :sType:',
				'bind' => array(
					'sType' => $sType
				)
			));

			if($oMediaCrop === false) {
				echo('not found');
				die;
			} else {
				if($this->iOriginalId > 0 && $sType == 'view') {
					$id = $this->iOriginalId;
				} else {
					$id = $this->id;
				}
				if($iLocaleId !== null) {
					$oLocales = $this->getLocales(array(
						'conditions' => 'iLocaleId = :iLocaleId:',
						'bind' => array(
							'iLocaleId' => $iLocaleId
						)
					));
					$sTitle = $oLocales[0]->sTitle;
					$sAlt = $oLocales[0]->sAlt;
				} else {
					$sTitle = '';
					$sAlt = '';
				}
				$sDate = $oMediaCrop->dtUpdated;
				$aInfo = array(
					'src' => strtolower('public/modules/'.$sModel.'/media/'.$sType.'/'.$id.'.'.$this->sExtension.'?date='.$sDate));
				if($sTitle != '') {
					$aInfo['title'] = $sTitle;
				}
				if($sAlt != '') {
					$aInfo['alt'] = $sAlt;
				}
				if($bWidth == true) {
					$aInfo['width'] = $oMediaCrop[0]->iCropWidth;
				}

				if($bHeight == true) {
					$aInfo['height'] = $oMediaCrop[0]->iCropHeight;
				}

				return $aInfo;
			}
		}

		/**
		 * @return bool
		 * Check if the media is video
		 */
		public function isVideo() {
			if($this->sUrl != '' && $this->sVideoId !== '') {
				return true;
			} else {
				return false;
			}
		}
	}