var html5upload = {
	id: 0,
	model: '',
	url: '',
	type: '',
	types: ['image','video'],
	formId: '',
	multiple: true,
	demo: false,
	toggleForm: function() {
		var el;
		for(var i = 0; i < this.types.length; i++) {
			el = $(this.types[i] + 'Upload');
			if(this.types[i] == this.type) {
				el.style.display = '';
				this.formId = this.type + 'Upload';
			} else {
				el.style.display = 'none';
			}
		}
	},
	//Adds a new input field for the IE < 11 fallback
	addFileInput: function(parent, name) {
		var parent = document.getElementById(parent);
		var wrapper = document.createElement('div');
		var el = document.createElement('input');
		el.type = 'file';
		el.name = name;
		wrapper.appendChild(el);
		parent.appendChild(wrapper);
		this.listFiles(null);
	},
	//Checks if the browsers supports the multiple uploads
	checkInputMultiple: function() {
		var el = document.createElement('input');
		el.type = 'file';
		var multiple = ('multiple' in el);
		if(multiple === false) {
			var fallback = document.getElementById(this.formId + 'Fallback');
			fallback.style.display = 'block';
		}
		this.multiple = multiple;
	},
	//Fired when the user has selected the files to be uploaded
	//It updates the UL-list showing the filenames
	listFiles: function(input) {
		/*if(this.multiple === false) {
			return false;
		}*/
		var list = document.getElementById(input.id +'List');

        //empty list for now...
        while (list.hasChildNodes()) {
            list.removeChild(list.firstChild);
        }

        //for every file...
        for (var x = 0; x < input.files.length; x++) {
            //add to list
            var li = document.createElement('li');
                li.innerHTML = input.files[x].name;
                list.appendChild(li);
        }
    },
	addVideo: function() {
		var form = document.getElementById(this.formId);
		if(form) {
			var href = this.url;
			var oUrl = document.getElementById(this.formId+'Url');
			var sUrl = oUrl.value;
			if(sUrl == '') {
				alert('You need to enter a URL');
			} else {
				var fd = new FormData();
				fd.append('ajax',1);
				fd.append('formId',this.formId);
				fd.append('id',this.id);
				fd.append('model',this.model);
				fd.append('sUrl', sUrl);
				var xhr = new XMLHttpRequest();
				xhr.addEventListener('load', this.videoComplete.bind(this), false);
				xhr.addEventListener('error', this.videoFailed, false);
				xhr.addEventListener('abort', this.videoCanceled, false);
				/* Be sure to change the url below to the url of your upload server side script */
				xhr.open('POST', href);
				xhr.send(fd);
			}
		} else {
			alert('video form not found');
		}
		return false;
	},
	videoComplete: function(evt) {
		/* This event is raised when the server send back a response */
		//Add the thumbnails to the overview
		var aFiles = JSON.decode(evt.target.responseText);
		this.addUploadsToElement(aFiles);
	},
	videoFailed: function() {
		alert('video processing failed');
	},
	videoCanceled: function() {
		alert('video processing cancelled');
	},
	//Prepare the formdata object used for the Ajax POST call
    //Only does file inputs for now
    createFormData: function() {
        var form = document.getElementById(this.formId);
        if(form) {
            var inputs = form.getElementsByTagName('input');
            var fd = new FormData();
            fd.append('ajax',1);
            fd.append('model',this.model);
            fd.append('id',this.id);
            var count = inputs.length;
            for(var i = 0; i < count; i++) {
                if(inputs[i].type.toLowerCase() == 'file') {
                //if(inputs[i].type.toLowerCase() == 'file' && inputs[i].multiple === true) {
                    var fileCount = inputs[i].files.length;
                    for(var j = 0; j < fileCount; j++) {
                        fd.append(this.formId + 'Files[]', inputs[i].files[j]);
					}
				}
			}
			return fd;
		} else {
			alert('form not found');
			return false;
		}
	},
	//Upload the files using AJAX and track progress
	uploadFiles: function() {
		if(this.multiple === true) {
			var href = this.url;
			var xhr = new XMLHttpRequest();
			var fd = this.createFormData();
			/* event listeners */
			xhr.upload.addEventListener('progress', this.uploadProgress.bind(this), false);
			xhr.addEventListener('load', this.uploadComplete.bind(this), false);
			xhr.addEventListener('error', this.uploadFailed, false);
			xhr.addEventListener('abort', this.uploadCanceled, false);
			/* Be sure to change the url below to the url of your upload server side script */
			xhr.open('POST', href);
			xhr.send(fd);
			return false;
		} else {
			return true;
		}
	},
	uploadCanceled: function(evt) {
		alert('The upload has been canceled by the user or the browser dropped the connection.');
	},
	uploadComplete: function(evt) {
		/* This event is raised when the server send back a response */
		//Add the thumbnails to the overview
		var aFiles = JSON.decode(evt.target.responseText);
		//Reset the input, list and progress bar
		var input = document.getElementById(this.formId + 'Files');
		input.value = '';
		var list = document.getElementById(this.formId + 'FilesList');
		list.innerHTML = '';
		var progress = document.getElementById(this.formId + 'Progress');
		progress.value = 0.0;

		//TODO: Add images to element
		this.addUploadsToElement(aFiles);
	},
	addUploadsToElement: function(aFiles)  {
		var documentFragment = new DocumentFragment();
		// Get the HTML element to set the output
		var id = this.model+this.id+'Items';
		var items = $(id);
		var images = [];

		for(var i = 0; i < aFiles.length; i++) {
			var div = document.createElement('div');
			div.className = 'media-wrapper';
			div.innerHTML = aFiles[i];
			images[i] = documentFragment.appendChild(div);
		}
		items.appendChild(documentFragment);
		for(var i = 0; i < images.length; i++) {
			var img = images[i].getElementsByTagName('img');
			if(img.length == 2) {
				if(img[1].className == 'cropper') {
					oChaniObjects['cropper'].assignClick(img[1]);
				}
			}
			oChaniObjects['media'].assignEventsToMedia(images[i], true);
		}
	},
	uploadFailed: function(evt) {
		alert('There was an error attempting to upload the files.');
	},
	//Update the progress bar
	uploadProgress: function(evt) {
		if (evt.lengthComputable) {
			var id = this.formId + 'Progress';
			document.getElementById(id).value = Math.round(evt.loaded * 100 / evt.total);
		}
	}
};

function updateLibrary() {
	var aFiles = null;
	if(aFiles !== null) {
		html5upload.addUploadsToLibrary(aFiles);
	}
}
updateLibrary();