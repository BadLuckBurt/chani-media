
var chaniMedia = new Class({
	Extends: chaniBlueprintCms,
	initialize: function(options) {
		this.parent(options);
		var el = $('mediaVideoWidth');
		el.addEvent('blur', function(el) {
			var iFrame = $('mediaIframe' + this.id);
			var width = el.value;
			var target = $('iVideoWidth' + this.id);
			target.value = width;
			if(iFrame) {
				iFrame.width = width;
			}
			this.saveValue(target);
		}.bind(this, el));

		el = $('mediaVideoHeight');
		el.addEvent('blur', function(el) {
			var height = el.value;
			var target = $('iVideoHeight' + this.id);
			target.value = height;
			this.saveValue(target);
		}.bind(this, el));

		el = $('mediaTitle');
		el.addEvent('blur', function(el) {
			var title = el.value;
			var target = $('sMediaTitle' + this.id);
			target.value = title;
			this.saveValue(target);
		}.bind(this, el));

		el = $('mediaAlt');
		el.addEvent('blur', function(el) {
			var title = el.value;
			var target = $('sMediaAlt' + this.id);
			target.value = title;
			this.saveValue(target);
		}.bind(this, el));

		var aMedia = $$('.media-wrapper');
		this.assignEventsToMedias(aMedia, false);
		oChaniObjects['media'] = this;
	},
	handleEvent: function(el, event, params) {
		var fxSuccess = null;
		switch (params[0]) {
			case 'deleteMedia':
				el.addEvent(event, function(el) {
					this.deleteMedia(el);
					return false;
				}.bind(this, el));
				break;
			case 'openCropper':
				el.addEvent(event, function(el) {
					this.openCropper(el);
					return false;
				}.bind(this, el));
				break;
			default:
				this.parent(el, event, params);
				break;
		}
	},
	assignEventsToMedias: function(aEls, bAjax) {
		for(var i = 0; i < aEls.length; i++) {
			this.assignEventsToMedia(aEls[i], bAjax);
		}
	},
	assignEventsToMedia: function(media, bAjax) {
		var edit = media.getElement('.media-edit');
		if(edit) {
			edit.addEvent('click',function(el) {
				this.id = el.getAttribute('data-id');
				Chani.toggleModalContent(el, true, false);
				return false;
			}.bind(this, edit));
		}
		var aDeleteButtons = media.getElements('.media-delete');
		for(var i = 0; i < aDeleteButtons.length; i++) {
			aDeleteButtons[i].addEvent('click',function(el) {
				this.deleteMedia(el);
				return false;
			}.bind(this, aDeleteButtons[i]));
		}
		var thumb = media.getElement('img');
		if(thumb) {
			if(!thumb.hasClass('video')) {
				thumb.addEvent('click', function (el) {
					oChaniObjects['cropper'].loadFromButton(el);
				}.bind(this, thumb));
			}
		}
	},
	assignEventsToMediaSettings: function(aEls) {
		for(var j = 0; j < aEls.length; j++) {
			this.assignEventsToEl(aEls[j]);
		}
	},
	deleteMedia: function(el) {
		if(confirm('Are you sure you want to delete this media?')) {
			var id = el.getAttribute('data-id');
			var req = new Request({
				url: this.options.ajax.deleteMediaUrl,
				data: {
					id: id
				},
				onSuccess: function(txt) {
					var thumb = $('media_' + this.getAttribute('data-id'));
					var mediaWrapper = thumb.parentNode.parentNode;
					mediaWrapper.parentNode.removeChild(mediaWrapper);
				}.bind(el)
			}).send();
		}
		return false;
	},
	getMediaSettings: function(el) {
		var id = el.getAttribute('data-id');
		var req = new Request({
			url: this.options.ajax.mediaSettingsUrl,
			data: {
				id: id
			},
			onSuccess: function(txt) {
				console.log(txt);
				return false;
				var caller = this.options.caller;
				var el = new Element('div');
				el.className = 'mediaSettings';
				el.innerHTML = txt;
				var targetId = this.options.el.getAttribute('data-model') + '_' + this.options.el.getAttribute('data-model-id') + 'Fields';
				var targetEl = $(targetId);
				while(targetEl.lastChild) {
					targetEl.removeChild(targetEl.lastChild);
				}
				el = targetEl.appendChild(el);
				var aEls = $(el).getElements('input.'+caller.options.className + ', select.'+caller.options.className + ', textarea.'+caller.options.className + ', button.'+caller.options.className);
				caller.assignEventsToMediaSettings(aEls);
			},
			el: el,
			caller: this
		}).send();
	},
	openCropper: function(el) {
		oChaniObjects['cropper'].loadFromButton(el);
	},
	assignValues: function() {
		var videoWidth = $('iVideoWidth' + this.id);
		if(videoWidth) {
			$('mediaUrl').innerHTML = $('sUrl' + this.id).value;
			$('mediaVideoWidth').value = videoWidth.value;
			$('mediaVideoHeight').value = $('iVideoHeight' + this.id).value;
		} else {
			$('mediaTitle').value = $('sMediaTitle' + this.id).value;
			$('mediaAlt').value = $('sMediaAlt' + this.id).value;
		}
	},
	saveMediaValues: function() {
		console.log(this.id);
	}
});